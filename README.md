<a name="To-Windows-APP-Candidate"></a>

# To Windows APP Candidate (Transcend)

Hi ,

Thanks for your interest in the position.  

We have a Windows APP exam (C#) before interview. Please finish it and commit your code to GitLab before interview. 

Please create a new branch for your commit. **You may need the permission to commit and create branch, please provide your GitLab account to us.**

<a name="Getting-Started"></a>

## Getting Started

- Please install Visual Studio Express first.  
[https://visualstudio.microsoft.com/zh-hant/vs/express/](https://visualstudio.microsoft.com/zh-hant/vs/express/)

- Please go GitLab to register an account and clone the following project.  
[https://gitlab.com/ts-candidate-windows/sample-app](https://gitlab.com/ts-candidate-windows/sample-app)

- Open the solution file: SampleApp.sln

In this APP, you have to finish the 3 functions :

<a name="1-XML-Parser-"></a>

## 1\. XML Parser :

There is a XML file on the web : https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/product.xml  
Please implement a function to parser the content and fill the value (SN/PN/IO/FW) of **SATA interfce** device on the TextBlock in the APP.

**Expected result :  
Tap "Get Info" button. The App get the value of XML and fill on the TextBlock.**

![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/winexam1.png)

## 2\. Disk Enumerate 

Please implement a function to list all disk letters of all interface on current computer.
Show the result on the ComboBox in the APP

**Expected result :  
Tap "List Disks" button. The App add disk letters to the ComboBox, and can be viewed through the drop-down menu**

![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/winexam2.png)

## 3\. Disk Capacity 

Please implement a function to obtain the disk capacity information of the **system disk**.
Show the cpacity information on the TextBlock and Progress Bar.

**Expected result :  
Tap "Get Capacity" button. The App get the disk Used/Total size of system disk in gigabyte and fill on the TextBlock and progress bar.**

![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/winexam3.png)
